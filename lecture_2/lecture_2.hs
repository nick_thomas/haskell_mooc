

repeatHelper 0 _ result = result
repeatHelper n str result = repeatHelper (n-1) str (result ++ str)



repeatString n str = repeatHelper n str ""


fibonacci :: Integer -> Integer
fibonacci n = fibonacci' 0 1 n

fibonacci' :: Integer -> Integer -> Integer-> Integer
fibonacci' a b 1 = b
fibonacci' a b n = fibonacci' b (a+b) (n-1)

describe:: Int -> String
describe n
 | n==2 = "Two"
 | even n = "Even"
 | n ==3 = "Three"
 | n> 100 = "Big!!"
 | otherwise = "The number " ++ show n


factorial :: Integer -> Integer
factorial n
 | n < 0 = -1
 | n == 0 = 1
 | otherwise = n * factorial (n-1)


guessAge :: String -> Int -> String
guessAge "Griselda" age
 | age < 47 = "Too low"
 | age > 47 = "Too high!"
 | otherwise = "Correct!"
guessAge "Hansel" age
 | age < 12 = "Too low!"
 | age > 12 = "Too high!"
 | otherwise = "Correct!"
guessAge name age = "Wrong name!"


f xs = take 2 xs ++ drop 4 xs

g xs = tail xs ++ [head xs]

login :: String -> Maybe String
login "f4bulous!" = Just "unicorn73"
login "swordfish" = Just "megahacker"
login _           = Nothing

perhapsMultiply :: Int -> Maybe Int -> Int
perhapsMultiply i Nothing = i
perhapsMultiply i (Just j) = i * j

intOrZero :: Maybe Int -> Int
intOrZero Nothing = 0
intOrZero (Just i) = i

safeHead :: [a] -> Maybe a
safeHead xs = if null xs then Nothing else Just (head xs)

headOrZero :: [Int] -> Int
headOrZero xs = intOrZero (safeHead xs)

readInt :: String -> Either String Int
readInt "0" = Right 0
readInt "1" = Right 1
readInt s = Left ("Unsupported string " ++ s)

iWantString :: Either Int String -> String
iWantString (Right str) = str
iWantString (Left number) = show number

describe2 :: Integer -> String
describe2 n = case n of 0 -> "zero"
                        1 -> "one"
                        n -> "the number " ++ show n

parseCountry :: String -> Maybe String
parseCountry "FI" = Just "Finland"
parseCountry "SE" = Just "Sweden"
parseCountry _ = Nothing

flyTo :: String -> String
flyTo countryCode = case parseCountry countryCode of Just country -> "You're flying to " ++ country
                                                     Nothing -> "You're not flying anymore"
 
getElement :: Maybe Int -> [a] -> a
getElement (Just i) xs = xs !! i
getElement Nothing xs = last xs

direction :: Either Int Int -> String
direction (Left i) = "you should go left" ++ show i ++ " meters! "
direction (Right i) = "you should go right " ++ show i ++ " meters!"


