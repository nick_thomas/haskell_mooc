import Data.List

applyTo1 :: (Int -> Int) -> Int
applyTo1 f = f 1

addThree :: Int -> Int
addThree x = x + 3

doTwice :: (a -> a) -> a -> a
doTwice f x = f (f x)

makeCool :: String -> String
makeCool str = "WOW " ++ str ++ "!"

positive :: Int -> Bool
positive x = x>0

onlyPositive xs = filter positive xs
mapBooleans f = map f [False, True]

wrapJust xs = map Just xs

palindrome :: String -> Bool
palindrome str = str == reverse str

palindromes :: Int -> [String]
palindromes n = filter palindrome (map show [1..n])

countAWords :: String -> Int
countAWords string = length (filter startsWithA (words string))
 where startsWithA s = head s == 'a'

substringsOfLength :: Int -> String -> [String]
substringsOfLength n string = map shorten (tails string)
 where shorten s = take n s

whatFollows :: Char -> Int -> String -> [String]
whatFollows c k string = map tail (filter match (substringsOfLength (k+1) string))
 where match sub = take 1 sub == [c]

between:: Integer  -> Integer -> Integer -> Bool
between lo high x = x < high && x > lo

doTwiceC :: (a -> a) -> a -> a
doTwiceC f = f . f

whatFollowsRedux :: Char -> Int -> String -> [String]
whatFollowsRedux c k = map tail . filter ((==[c]) . take 1) .map (take (k+1)) . tails

findSubString :: String -> String -> String
findSubString chars = takeWhile (\x -> elem x chars)
 . dropWhile (\x-> not $ elem x chars)

descend 0 = []
descend n = n : descend(n-1)

iterateme f 0 x = [x]
iterateme f n x = x : iterateme f (n-1) (f x)

myhead :: [Int] -> Int
myhead [] = -1
myhead (first:rest) = first

mytail :: [Int] -> [Int]
mytail [] = []
mytail (first:rest) = rest

describeList :: [Int] -> String
describeList []  = "an empty list"
describeList (x: []) = "a list with one element"
describeList (x:y: []) = "a list with two elements"
describeList (x:y:z:xs) = "a list with atleast three elements"

sumNumbers :: [Int] -> Int
sumNumbers [] = 0
sumNumbers (x:xs) = x + sumNumbers xs

doubleList :: [Int] -> [Int]
doubleList [] = []
doubleList (x: xs) = 2 * x : doubleList xs

(<+>) :: [Int]-> [Int]-> [Int]
xs <+> ys = zipWith (+) xs ys

(+++) :: String -> String -> String
a +++ b = a ++ " " ++ b
